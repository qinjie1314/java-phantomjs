package cn.thecover.phantomjs.vo;

/**
 * ReturnCodes
 *
 * @author bard
 * @date 2019/08/25
 */

public enum ReturnCodes {

    RESPONSE_CODE_PARAM_ERROR(400, "参数错误"),

    RESPONSE_CODE_HINT(501, "校验失败"),
    /**
     * 响应成功
     */
    RESPONSE_CODE_SUCCESS(200, "响应成功");


    private int code;
    private String message;

    ReturnCodes(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public static String getMessage(Integer code) {
        for (ReturnCodes item : ReturnCodes.values()) {
            if (item.getCode() == code) {
                return item.getMessage();
            }
        }
        return code.toString();
    }


    public void setMessage(String message) {
        this.message = message;
    }
}
