package cn.thecover.phantomjs.vo;


import java.util.Collections;
import java.util.List;

/**
 * @author focus
 * @since 2020年08月13日 14:48
 **/
public class PageInfoResultVO<T> {
    public static final int DEFAULT_PAGE_START = 1;
    public static final int DEFAULT_PAGE_SIZE = 20;

    /**
     * 列表数据
     */
    List<T> list;
    /**
     * 页码
     */
    Integer pageNum;
    /**
     * 页大小
     */
    Integer pageSize;
    /**
     * 记录总条数
     */
    Long total;

    Long totalPages;

    Object other;

    public PageInfoResultVO(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.total = 0L;
        this.totalPages = 0L;
        this.list = Collections.emptyList();
    }

}