package cn.thecover.phantomjs.vo;

import org.springframework.util.StringUtils;

/**
 * @author bard
 * @date 2019/08/25
 */
public class RestResponse<T> {
    public RestResponse() {
    }

    private int rspcode = ReturnCodes.RESPONSE_CODE_SUCCESS.getCode();
    private String rspdesc = ReturnCodes.RESPONSE_CODE_SUCCESS.getMessage();
    private T data;

    public void setRespByCodes(ReturnCodes codes) {
        this.rspcode = codes.getCode();
        this.rspdesc = codes.getMessage();
    }

    public RestResponse<T> setData(T data) {
        this.data = data;
        return this;
    }

    public RestResponse(ReturnCodes codes) {
        this.rspcode = codes.getCode();
        this.rspdesc = codes.getMessage();
    }

    public RestResponse(ReturnCodes codes, T data) {
        this.rspcode = codes.getCode();
        this.rspdesc = codes.getMessage();
        this.data = data;
    }

    public int getRspcode() {
        return rspcode;
    }

    public void setRspcode(int rspcode) {
        this.rspcode = rspcode;
    }

    public String getRspdesc() {
        return rspdesc;
    }

    public void setRspdesc(String rspdesc) {
        this.rspdesc = rspdesc;
    }

    public void setReturnCode(ReturnCodes returnCode) {
        this.rspcode = returnCode.getCode();
        this.rspdesc = returnCode.getMessage();
    }

    public T getData() {
        return data;
    }

    public static RestResponse<Object> ok() {
        return new RestResponse<>(ReturnCodes.RESPONSE_CODE_SUCCESS);
    }

    public static <T> RestResponse<T> ok(T data) {
        return new RestResponse<>(ReturnCodes.RESPONSE_CODE_SUCCESS, data);
    }


    public static RestResponse<Object> fail(int rspCode, String msg) {
        RestResponse<Object> response = new RestResponse<Object>();
        response.setRspcode(rspCode);
        if (StringUtils.hasText(msg)) {
            response.setRspdesc(msg);
        }
        return response;
    }
}
