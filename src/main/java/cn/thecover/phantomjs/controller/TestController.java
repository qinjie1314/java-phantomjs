package cn.thecover.phantomjs.controller;

import cn.thecover.phantomjs.vo.RestResponse;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.net.ssl.SSLContext;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.*;


@RestController
public class TestController {
    @Bean
    public RestTemplate restTemplate(HttpComponentsClientHttpRequestFactory factory) {
        RestTemplate restTemplate = new RestTemplate(factory);
        // 支持中文编码
        restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        return restTemplate;
    }
    @Bean
    public HttpComponentsClientHttpRequestFactory sslIgnoreFactory() throws Exception {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        SSLContext sslContext = SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();
        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        requestFactory.setConnectTimeout(15000);
        requestFactory.setReadTimeout(5000);
        return requestFactory;
    }


    @Value("${application.workpath}")
    private String workpath;
    @Resource
    private RestTemplate restTemplate;
    HttpHeaders headers = new HttpHeaders(){{
        setContentType(MediaType.APPLICATION_JSON);
        setAcceptCharset(Collections.singletonList(StandardCharsets.UTF_8));
    }};
    @PostMapping("/echartImage")
    RestResponse<Object> echartImage(@RequestBody Map<String, String> obj) {
        List<String> path = Collections.singletonList(workpath + System.currentTimeMillis() + "_" + getRandomString(5) + ".txt");
        List<String> options = Collections.singletonList(obj.get("option"));
        Map<String, Object> params = new HashMap<>(4);
        params.put("path", path);
        params.put("option", options);

        HttpEntity<String> entity = new HttpEntity<>(JSONObject.toJSONString(params), headers);
        // 替换成你的请求URL
        String url = "http://127.0.0.1:8081";
        restTemplate.postForObject(url, entity, String.class);
        StringBuilder res = new StringBuilder();
        try {
            BufferedReader br = null;
            InputStreamReader isr = null;
            FileInputStream fis = null;
            String filePath = path.get(0);
            try {
                // 指定文件路径
                File file = new File(filePath);
                Thread.sleep(300);
                if (!file.exists() || file.length() == 0){
                    Thread.sleep(300);
                }
                if (!file.exists() || file.length() == 0){
                    Thread.sleep(300);
                }
                if (!file.exists() || file.length() == 0){
                    Thread.sleep(300);
                }
                if(file.exists() && file.length() != 0){
                    // 创建输入流
                    fis = new FileInputStream(file);
                    isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
                    br = new BufferedReader(isr);

                    // 逐行读取文件内容
                    String line;
                    while ((line = br.readLine()) != null) {
                        res.append(line);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if(br != null){
                    br.close();
                }
                if(isr != null){
                    isr.close();
                }
                if(fis != null){
                    fis.close();
                }
                new File(filePath).delete();
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        return RestResponse.ok(res.toString());
    }

    public static void main(String[] args) {
        File file = new File("C:\\Users\\PC\\Desktop\\1.txt");
        if(file.exists()){
            System.out.println(file);
            System.out.println(file.length());
        }
    }

    public static String getRandomString(int length) { //length表示生成字符串的长度

        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
}
