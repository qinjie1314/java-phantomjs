FROM registry.cn-chengdu.aliyuncs.com/qinjie/jdk1.8
COPY phantomjs  phantomjs/
COPY target/phantomjs-0.0.1.jar app.jar
ENV TZ=Asia/Shanghai
ENV ENV=stg
RUN apt-get update -y
RUN tar -jxvf phantomjs/phantomjs211.tar.bz2
RUN rm phantomjs/phantomjs211.tar.bz2
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
# Echarts支持中文
RUN chmod 777 phantomjs/run.sh
RUN apt-get install xfonts-wqy fontconfig -y
# CMD ["nohup", "java", "-jar", "app.jar", "&"]

CMD ["./phantomjs/run.sh"]