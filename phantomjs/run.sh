#!/bin/bash
nohup phantomjs-2.1.1-linux-x86_64/bin/phantomjs phantomjs/echarts-convert/server.js 1>/dev/null 2>error.log &
java -jar -Xms128M -Xmx256M app.jar --spring.profiles.active=$ENV