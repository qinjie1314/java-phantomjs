var webserver = require('webserver').create()
var webpage = require('webpage')

webserver.listen(8081, function (request, response) {
    // 这里一定要用异常捕获来应对诸多错误问题（请求参数错误、奔溃、阻塞等），不然会阻塞进程导致不可用
    var system = require('system');
    try {
        // 如果请求不合法则结束
        if (request.post === undefined || request.post.length === 0) {
            throw "Request is illegal"
        }
        var postData = JSON.parse(request.post) // Post数据
        // 如果不是一个合法的JSON对象
        if (typeof postData != 'object') {
            throw "not json object"
        }
        var config = {
            // define the location of js files
            //JQUERY : '../js/jquery.js',
            JQUERY : 'echarts/jquery-1.9.1.min.js',
            //ECHARTS3 : '../../thirdlib/layuiadmin/scripts/echarts-4.4.0-rc.1.js',
            ECHARTS3 : 'echarts/echarts-4.4.0-rc.1.js',
            // ECHARTS3 : 'echarts.min_4.js',
            // default container width and height
            DEFAULT_WIDTH : '1000',
            DEFAULT_HEIGHT : '600'
        }, parseParams, render;
        var baseData = postData;
        // var baseData = postData;
        usage = function(m)
        {
            console.log( m + "Usage: phantomjs echarts-convert.js -txtPath path1 -picTmpPath path2 -picPath path3 -width width -height height\n");
        };

        errExist = function()
        {
            console.log("exist error, will do phantom exist...");
        }

        pick = function()
        {
            try
            {
                var args = arguments, i, arg, length = args.length;
                for (i = 0; i < length; i += 1)
                {
                    arg = args[i];
                    if (arg !== undefined && arg !== null && arg !== 'null' && arg != '0')
                    {
                        return arg;
                    }
                }
            } catch (e)
            {
                console.log(" Err :" + e);
                errExist();
            }
        };
        var base64Pad = '=';
        var toBinaryTable = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53,
            54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, 0, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
            14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34,
            35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1
        ];
        base64ToString = function(data)
        {
            // console.log("data = "+data);
            var result = '';
            try
            {
                var leftbits = 0; // number of bits decoded, but yet to be appended
                var leftdata = 0; // bits decoded, but yet to be appended
                // Convert one by one.
                for (var i = 0; i < data.length; i++)
                {
                    var c = toBinaryTable[data.charCodeAt(i) & 0x7f];
                    // console.log("i = "+c);
                    var padding = (data.charCodeAt(i) == base64Pad.charCodeAt(0));
                    // Skip illegal characters and whitespace
                    if (c == -1)
                        continue;

                    // Collect data into leftdata, update bitcount
                    leftdata = (leftdata << 6) | c;
                    leftbits += 6;

                    // If we have 8 or more bits, append 8 bits to the result
                    if (leftbits >= 8)
                    {
                        leftbits -= 8;
                        // Append if not padding.
                        if (!padding)
                            result += String.fromCharCode((leftdata >> leftbits) & 0xff);
                        leftdata &= (1 << leftbits) - 1;
                    }
                }

                console.log(result);
                // if (leftbits)
                // throw Components.Exception('Corrupted base64 string');
            } catch (e)
            {
                console.log("base64ToString() Err :" + e);
                errExist();
            }

            return result;
        };


        render = function(params)
        {
            var page = undefined;
            var createChart;
            var base64 = [];

            try
            {
                page = require('webpage').create(), createChart;

                page.onConsoleMessage = function(msg)
                {
                    console.log("Console:【" + msg + "】");
                };

                page.onAlert = function(msg)
                {
                    console.log("Alert:【" + msg + "】");
                };

                createChart = function(inputOption, width, height)
                {
                    var counter = 0;
                    function decrementImgCounter()
                    {
                        counter -= 1;
                        if (counter < 1)
                        {
                            console.log("decrementImgCounter() : " + messages.imagesLoaded);
                        }
                    }

                    function loadScript(varStr, codeStr)
                    {
                        try
                        {
                            var script = $('<script>').attr('type', 'text/javascript');
                            // script.html('var ' + varStr + ' = ' + codeStr);
                            script.html(codeStr);
                            document.getElementsByTagName("head")[0].appendChild(script[0]);
                            if (window[varStr] !== undefined)
                            {
                                console.log('Echarts.' + varStr + ' has been parsed');
                            }
                        } catch (e)
                        {
                            console.log("loadScript() Err :" + e);
                            errExist();
                        }
                    }

                    function loadImages()
                    {
                        try
                        {
                            var images = $('image'), i, img;
                            if (images.length > 0)
                            {
                                counter = images.length;
                                for (i = 0; i < images.length; i += 1)
                                {
                                    img = new Image();
                                    img.onload = img.onerror = decrementImgCounter;
                                    img.src = images[i].getAttribute('href');
                                }
                            } else
                            {
                                console.log('loadImages() : The images have been loaded complete');
                            }
                        } catch (e)
                        {
                            console.log("loadImages() Err :" + e);
                            errExist();
                        }
                    }

                    // load opiton
                    if (inputOption != 'undefined')
                    {
                        // parse the option
                        loadScript('option', inputOption);
                        // disable the animation
                        option.animation = false;
                    }

                    // we render the image, so we need set background to white.
                    $(document.body).css('backgroundColor', 'white');
                    var container = document.getElementById('container');
                    if(!container)
                    {
                        container = $("<div>").appendTo(document.body);
                        container.attr('id', 'container');
                        container.css({
                            width : width,
                            height : height
                        });
                    }

                    // render the chart
                    var myChart = echarts.getInstanceByDom(document.getElementById("container"));
                    if(!myChart)
                    {
                        // 防止重复初始化
                        myChart = echarts.init(document.getElementById("container"));
                    }
                    myChart.clear();
                    myChart.setOption(option);
                    // load images
                    loadImages();
                };

                // parse the params
                page.open("about:blank", function(status)
                {
                    console.log("------------start page.open------------")
                    // inject the dependency js
                    page.injectJs(config.JQUERY);
                    page.injectJs(config.ECHARTS3);

                    for (var i = 0; i < params['option'].length; i++)
                    {
                        var width = params['width'] ? params['width'][i] : config.DEFAULT_WIDTH;
                        var height = params['height'] ? params['height'][i] : config.DEFAULT_HEIGHT;

                        // define the clip-rectangle
                        page.clipRect = {
                            top : 0,
                            left : 0,
                            width : width,
                            height : height
                        };

                        // create the chart
                        page.evaluate(createChart, params['option'][i], width, height);
                        // render the image
                        var fs = require('fs');

                        fs.write(baseData['path'][i], page.renderBase64('PNG'), 'w');
                    }
                });
            } catch (e)
            {
                console.log("render() Err :" + e);
                errExist();
            }finally {
                if(page !== undefined){
                    page.close()
                }
            }
            return base64;
        };

        try
        {
            render(baseData)
        } catch (e)
        {
            console.log("main() has catch Err:" + e)
            errExist();
        }
        response.write("");
        console.log("success")
        response.close() // 这里需要结束响应，不然连接会一直占用
    } catch (e) {
        console.log('error', e)
        // TODO 这了可以做一些错误日志
        response.statusCode = 200
        response.write(J(0, e.toString()))
        response.close()
    }
})
console.log('Service started...')

function J(code, msg, data) {
    if (data === undefined) {
        data = {}
    }
    var t = new Date().getTime()
    return JSON.stringify({
        code: code,
        msg: msg,
        data: data,
        timestamp: t,
    })
}
function randomNum(n){
    var res = "";
    for(var i=0;i<n;i++){
        res += Math.floor(Math.random()*10);
    }
    return res;
}